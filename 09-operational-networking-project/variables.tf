
variable "vpc_cidr" {
  type        = string
  description = "vpc cidr"
  default     = "t2.micro"
}

variable "public_subnet_cidr" {
  type        = list
  description = "List of public subnets"
}

variable "public_instance_type" {
  type        = string
  description = "Public instance type"
  default     = "t2.micro"
}

variable "private_instance_type" {
  type        = string
  description = "private instance type"
  default     = "t2.micro"
}