
#################
## recap
#################


variable "key_name" {
    type = string
    description = "name of keypair to pull down using data source"
    default = "kojitechs-demo-key"
}

