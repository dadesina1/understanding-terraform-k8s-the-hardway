
variable "vpc_cidr" {
  type = string
  description = "VPC network cidr"
}

variable "public_subnet_cidr" {
  type = list
  description = "VPC public subnets cidr"
}

/* variable "public_subnet_2_cidr" {
  type = string
  description = "VPC subnet 2 cidr"
  default = "10.0.2.0/24"
} */

variable "private_subnet_cidr" {
  type = list
  description = "VPC private subnets cidr"
}

/* variable "private_subnet_2_cidr" {
  type = string
  description = "VPC private subnet 2 cidr"
  default = "10.0.3.0/24"
} */

variable "database_subnet_cidr" {
  type = list
  description = "VPC database subnets cidr"
}

/* variable "database_subnet_2_cidr" {
  type = string
  description = "VPC database subnet 2 cidr"
  default = "10.0.7.0/24"
} */

variable "public_instance_type" {
  type = string
  description = "public instance type"
  default = "t2.micro"
}

variable "private_instance_type" {
  type = string
  description = "private instance type"
  default = "t2.micro"
}

