########## Parent Module for S3##########


resource "aws_s3_bucket" "dabuk22" {
  bucket = var.bucket_name

  tags = var.tags
}

resource "aws_s3_bucket_acl" "dabuk22" {
  bucket = aws_s3_bucket.dabuk22.id
  acl    = "private" #SLA (surface level agreement)
}

resource "aws_s3_bucket_versioning" "dabucket22" {
  bucket = aws_s3_bucket.dabuk22.id
  versioning_configuration {
    status = var.versioning
  }
}