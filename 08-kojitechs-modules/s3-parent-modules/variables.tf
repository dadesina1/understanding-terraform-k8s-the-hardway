
variable "bucket_name" {
    type = string
    description = "s3 bucket name"
    # default # only use this if its manadatory
}

variable "tags" {
    type = map
    description = "tag value"
    default = {}
}

variable "versioning" {
    type = string
    description = "do you want to enable versioning"
}