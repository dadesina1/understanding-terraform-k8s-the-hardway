#!/bin/bash
sudo yum update -y
sudo yum install docker -y
sudo service docker start
sudo useradd docker
sudo usermod -aG docker docker 
sudo chmod 777 /var/run/docker.sock
sudo yum install git -y
sudo git clone https://gitlab.com/dadesina1/understanding-terraform-k8s-the-hardway.git
## Install kublet
sudo curl -O curl -O https://s3.us-west-2.amazonaws.com/amazon-eks/1.25.7/2023-03-17/bin/darwin/amd64/kubectl
sudo chmod +x ./kubectl
sudo mkdir -p $HOME/bin && cp ./kubectl $HOME/bin/kubectl && export PATH=$PATH:$HOME/bin
sudo echo 'export PATH=$PATH:$HOME/bin' >> ~/.bashrc
sudo kubectl version --short --client
