
######
# Get public IP 
#######

output "public_ip" {
    description = " fetch public ip" 
    value = aws_instance.web.public_ip
}