
variable "subnet_1_cidr" {
    description = "value for subnet cidr"
    type = string
    default = "10.0.1.0/24"
}

variable "subnet_2_cidr" {
    description = "value for subnet 1 cidr"
    type = string
    default = "10.0.2.0/24"
}

variable "subnet_1_az" {
    description = "value for subnet 1 az"
    type = string
    default = "us-east-1a"
}

variable "subnet_2_az" {
    description = "value for subnet 2 az"
    type = string
    default = "us-east-1b"
}

variable "vpc_cidr_block" {
    description = "vpc cidr block"
    type = string
    default = "10.0.0.0/16"
}