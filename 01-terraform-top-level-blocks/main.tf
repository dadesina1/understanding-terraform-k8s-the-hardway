
# creating vpc
resource "aws_vpc" "kojitechs_vpc" {
  cidr_block = var.vpc_cidr_block

  tags = {
    Name = "kojitechs-vpc"
  }
}  
 
## "Resource_name.local_name.desired_Artr"
resource "aws_subnet" "subnet_1" {
  vpc_id     = local.vpc_id
  cidr_block = var.subnet_1_cidr
  availability_zone = var.subnet_1_az

  tags = {
    Name = "subnet_1"
  }
}

resource "aws_subnet" "subnet_2" {
  vpc_id     = local.vpc_id
  cidr_block = var.subnet_2_cidr
  availability_zone = var.subnet_2_az

  tags = {
    Name = "subnet_2"
  }
}


resource "aws_instance" "web" {
  ami           = "ami-0dfcb1ef8550277af"
  instance_type = "t3.micro"

  tags = {
    Name = "web"
  }
}
