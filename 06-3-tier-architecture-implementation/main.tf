
locals {
  vpc_id = aws_vpc.that.id
  az     = data.aws_availability_zones.available.names
}

# local.az
resource "aws_vpc" "that" {
  cidr_block           = var.vpc_cidr
  enable_dns_support   = true
  enable_dns_hostnames = true

  tags = {
    Name = "that-vpc-${terraform.workspace}"
  }
}

resource "aws_internet_gateway" "gw" {
  vpc_id = local.vpc_id

  tags = {
    Name = "kojitechs-gw-${terraform.workspace}"
  }
}

resource "aws_subnet" "public_subnet_1" {
  count = length(var.public_subnet_cidr)

  vpc_id                  = local.vpc_id
  cidr_block              = var.public_subnet_cidr[count.index]
  availability_zone       = local.az[count.index]
  map_public_ip_on_launch = true

  tags = {
    Name = "public-subnet-1-${count.index + 1}-${terraform.workspace}"
  }
}

resource "aws_subnet" "public_subnet_2" {
   vpc_id                  = local.vpc_id
   cidr_block              = "10.0.2.0/24"
   availability_zone       = "us-east-1b" 
   map_public_ip_on_launch = true

   tags = {
     Name = "public-subnet-2-${terraform.workspace}"
   }
}

resource "aws_subnet" "private_subnet_1" {
  vpc_id            = local.vpc_id
  cidr_block        = "10.0.1.0/24"
  availability_zone = "us-east-1a"

  tags = {
    Name = "private-subnet-1-${terraform.workspace}"
  }
}

resource "aws_subnet" "private_subnet_2" {
  vpc_id            = local.vpc_id
  cidr_block        = "10.0.3.0/24"
  availability_zone = "us-east-1b"

  tags = {
    Name = "private-subnet-2-${terraform.workspace}"
  }
}

resource "aws_subnet" "database_subnet_1" {
  vpc_id            = local.vpc_id
  cidr_block        = "10.0.5.0/24"
  availability_zone = "us-east-1a"

  tags = {
    Name = "database-subnet-1-${terraform.workspace}"
  }
}

resource "aws_subnet" "database_subnet_2" {
  vpc_id            = local.vpc_id
  cidr_block        = "10.0.7.0/24"
  availability_zone = "us-east-1b"

  tags = {
    Name = "database-subnet-2-${terraform.workspace}"
  }
}

################################################################################
# CREATING PUBLIC ROUTE TABLES ASSOCIATED WITH PUBLIC SUBNET?
################################################################################

resource "aws_route_table" "public_route_table" {
  vpc_id = local.vpc_id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.gw.id
  }

  tags = {
    Name = "public-route-table"
  }
}

################################################################################
# CREATING PUBLIC ROUTE TABLES ASSOCIATION
################################################################################
resource "aws_route_table_association" "rt_association" {
  count = length(var.public_subnet_cidr)

  subnet_id      = aws_subnet.public_subnet.*.id[count.index]
  route_table_id = aws_route_table.public_route_table.id
}

resource "aws_route_table_association" "public_subnet_2" {
  subnet_id      = aws_subnet.public_subnet_2.id
  route_table_id = aws_route_table.public_route_table.id
}

################################################################################
# CREATING DEFAULT ROUTE TABLES 
################################################################################

resource "aws_default_route_table" "that" {
  default_route_table_id = aws_vpc.that.default_route_table_id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_nat_gateway.that.id
  }

  tags = {
    Name = "that"
  }
}

################################################################################
# CREATING NAT GATEWAY
################################################################################

resource "aws_nat_gateway" "that" {
  depends_on = [aws_internet_gateway.gw]

  allocation_id = aws_eip.eip.id
  subnet_id     = aws_subnet.public_subnet[0].id

  tags = {
    Name = "gw-NAT"
  }
}

################################################################################
# CREATING A AN  ELASTICIP
################################################################################ 

resource "aws_eip" "eip" {
  depends_on = [aws_internet_gateway.gw]
  vpc        = true
}