
terraform {
  required_version = ">=1.1.0"

  backend "s3" {
    bucket         = "06.3-tier.architecture-implentation"
    key            = "path/evn"
    region         = "us-east-1"
    dynamodb_table = "terraform-lock2"
    encrypt        = true
  }
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 4.0"
    }
  }
}


