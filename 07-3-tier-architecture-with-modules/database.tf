

resource "random_password" "password" {
    length = 16
    special = false
}

resource "aws_db_subnet_group" "mysql_subnetgroup" {
  name       = "registrationapp-subnetgroup-${terraform.workspace}"
  subnet_ids = [aws_subnet.database_subnet_1.id, aws_subnet.database_subnet_2.id]

  tags = {
    Name = "registrationapp-subnetgroup-${terraform.workspace}"
  }
}

locals {
    db_credentials = {
        endpoint = aws_db_instance.registration_app_db.address
        db_username = var.db_username
        db_name = var.db_name
        port = var.port
        password = random_password.password.result
    }

}

resource "aws_db_instance" "registration_app_db" {
  
  allocated_storage    = 20
  db_name              = "webappdb"
  engine               = "mysql"
  engine_version       = "5.7"
  instance_class       = var.instance_class
  username             = var.db_username
  port                 = var.port
  password             = random_password.password.result
  parameter_group_name = "default.mysql5.7"
  db_subnet_group_name = aws_db_subnet_group.mysql_subnetgroup.id
  skip_final_snapshot  = true
  vpc_security_group_ids = [aws_security_group.database_security_group.id]
}

resource "aws_secretsmanager_secret" "that" {
  name = "kojitechs-mysql-secret-${terraform.workspace}"
  description = "Secret to mysql database"
}

resource "aws_secretsmanager_secret_version" "mysql-secret" {
  secret_id     = aws_secretsmanager_secret.that.id
  secret_string = jsonencode(local.db_credentials)
}