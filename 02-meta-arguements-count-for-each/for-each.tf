
# creating vpc
resource "aws_vpc" "kojitechs_vpc" {
  cidr_block = var.vpc_cidr_block

  tags = {
    Name = "kojitechs-vpc"
  }
}  
 

locals {
  public_subnet = {
    pub_subnet_1 = {
        cidr_block ="10.0.0.0/24"
        az = "us-east-1a"
    }
     public_subnet_2 = {
    pub_subnet_2 = {
        cidr_block ="10.0.2.0/24"
        az = "us-east-1b"
    }
  }
}

variable "public_subnets" {
    type = map(any)
    description = "objects of public_subnets to be created"
    default = {
        public_subnet_1 = {
            cidr_block = "10.0.0.0/24"
            availability_zone = "us-east-1a"
        }
    } 
}


## "Resource_name.local_name.desired_Artr"
resource "aws_subnet" "public_subnets_for_each" {

for_each = local.public_subnet

  vpc_id     = local.vpc_id
  cidr_block = each.value.cidr_block
  availability_zone = each.value.az
  map_public_ip_on_launch = true

  tags = {
    Name = each.key
  }
}

resource "aws_subnet" "private_subnets_for_each" {
  count = 4

  vpc_id     = local.vpc_id
  cidr_block = var.private_subnet_cidr[count.index]
  availability_zone = data.aws_availability_zones.available.names[count.index]

  tags = {
    Name = private_subnets_for_each
  }
}

# resource "aws_subnet" "subnet_2" {
#  vpc_id     = local.vpc_id
#  cidr_block = var.subnet_2_cidr
#  availability_zone = var.subnet_2_az
#
#  tags = {
#    Name = "subnet_2"
#  }
# }


resource "aws_instance" "web" {
  ami           = "ami-0dfcb1ef8550277af"
  instance_type = "t3.micro"
  subnet_id = aws_subnet.public_subnets[1].id

  tags = {
    Name = "web"
  }
}