
#################
# Get public IP 
#################

output "public_ip" {
    description = " fetch public ip" 
    value = aws_instance.web.public_ip
}

# Fetch public subnet id
output "public_subnet_id_splat" {
    description = " fetch public subnet id" 
    value = aws_subnet.public_subnets[*].id
}

#output "public_subnet_id_legacy_splat" {
#    description = " fetch public subnet id" 
#    value = aws_subnet.public_subnets.id
#}