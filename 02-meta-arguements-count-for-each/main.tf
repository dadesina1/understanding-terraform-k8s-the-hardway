
###########################
# Count for meta arguements 
###########################

# creating vpc
resource "aws_vpc" "kojitechs_vpc" {
  cidr_block = var.vpc_cidr_block

  tags = {
    Name = "kojitechs-vpc"
  }
}  
 
## "Resource_name.local_name.desired_Artr"
resource "aws_subnet" "public_subnets" {
  count = length(var.public_subnet_cidr)

  vpc_id     = local.vpc_id
  cidr_block = var.public_subnet_cidr[count.index]
  availability_zone = data.aws_availability_zones.available.names[count.index]
  map_public_ip_on_launch = true

  tags = {
    Name = "public-subnets-${count.index + 1}"
  }
}

resource "aws_subnet" "private_subnets" {
  count = 4

  vpc_id     = local.vpc_id
  cidr_block = var.private_subnet_cidr[count.index]
  availability_zone = data.aws_availability_zones.available.names[count.index]

  tags = {
    Name = "private-subnets-${count.index + 1}"
  }
}

# resource "aws_subnet" "subnet_2" {
#  vpc_id     = local.vpc_id
#  cidr_block = var.subnet_2_cidr
#  availability_zone = var.subnet_2_az
#
#  tags = {
#    Name = "subnet_2"
#  }
# }


resource "aws_instance" "web" {
  ami           = "ami-0dfcb1ef8550277af"
  instance_type = "t3.micro"
  subnet_id = aws_subnet.public_subnets[1].id

  tags = {
    Name = "web"
  }
}
