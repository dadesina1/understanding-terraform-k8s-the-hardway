
terraform {
  
 backend "s3" {
  bucket = "kojitechs.state.bucket"
  key = "path/env"
  region = "us-east-1"
  dynamodb_table = "terraform-lock"
  encrypt = true
 }
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 4.0"
    }
  }
}