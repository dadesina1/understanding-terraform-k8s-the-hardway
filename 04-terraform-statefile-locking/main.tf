

resource "aws_s3_bucket" "that" {
  count = length(var.bucket_name)

  bucket = var.bucket_name[count.index]

  lifecycle {
    prevent_destroy = false
  }
}

resource "aws_dynamodb_table" "dynamodb-terraform-lock-2" {
  name           = "terraform-lock2"
  hash_key       = "LockID" # IMPORTANT 
  read_capacity  = 20
  write_capacity = 20

  attribute {
    name = "LockID"
    type = "S"
  }
  lifecycle {
    prevent_destroy = false
  }
}
