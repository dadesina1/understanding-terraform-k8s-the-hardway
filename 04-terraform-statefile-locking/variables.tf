

variable "bucket_name" {
  type        = list(any)
  description = "The name of the  state bucket"
  default = [
    "that.state.bucket",
    "05.terraform.workspace",
    "06.3-tier.architecture-implentation"
    ]
}
